#define DEBUG_SERIAL Serial
#define GPS_SERIAL Serial1
#define LED_OK 12
#define LED_WARN 11
#define LED_ERR 10
#define WIRE_BUS 3
#define airborneAlt 100000

// GPS
long lat, lon, alt;
char fixStatus, utcTime[15];
unsigned char antennaStatus, jammingIndicator;
unsigned short noiseLevel;
unsigned long lastUpdate, lastGpsPacket = millis();

bool airborne = false;
const byte portableCommand[] = {0xB5, 0x62, 0x06, 0x24, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x42, 0x71};
const byte airborneCommand[] = {0xB5, 0x62, 0x06, 0x24, 0x00, 0x01, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x3D};
const byte resetCommand[] = {0xB5, 0x62, 0x06, 0x04, 0x00, 0x01, 0x00, 0x00, 0x0B, 0x3B};

const byte UBX_HEADER[] = {0xB5, 0x62};
const byte NAV_POSLLH_HEADER[] = {0x01, 0x02};
const byte NAV_STATUS_HEADER[] = {0x01, 0x03};
const byte NAV_TIMEUTC_HEADER[] = {0x01, 0x21};
const byte MON_HW_HEADER[] = {0x0A, 0x09};

enum _ubxMsgType {
  MT_NONE,
  MT_NAV_POSLLH,
  MT_NAV_STATUS,
  MT_NAV_TIMEUTC,
  MT_MON_HW
};

struct NAV_POSLLH {
  unsigned char cls;
  unsigned char id;
  unsigned short len;
  unsigned long iTOW;
  long lon;
  long lat;
  long alt;
  long altMSL;
  unsigned long hAcc;
  unsigned long vAcc;
};

struct NAV_STATUS {
  unsigned char cls;
  unsigned char id;
  unsigned short len;
  unsigned long iTOW;
  unsigned char gpsFix;
  char flags;
  char fixStat;
  char flags2;
  unsigned long ttff;
  unsigned long msss;
};

struct NAV_TIMEUTC {
  unsigned char cls;
  unsigned char id;
  unsigned short len;
  unsigned long iTOW;
  unsigned long tAcc;
  signed long nano;
  unsigned short year;
  unsigned char month;
  unsigned char day;
  unsigned char hour;
  unsigned char minute;
  unsigned char second;
  unsigned char valid;
};

struct MON_HW {
  unsigned char cls;
  unsigned char id;
  unsigned short len;
  unsigned long pinSel;
  unsigned long pinBank;
  unsigned long pinDir;
  unsigned long pinVal;
  unsigned short noisePerMS;
  unsigned short agcCnt;
  unsigned char aStatus;
  unsigned char aPower;
  unsigned char flags;
  unsigned char reserved1;
  unsigned long usedMask;
  unsigned char VP[25];
  unsigned char jamInd;
  unsigned short reserved3;
  unsigned long pinIrq;
  unsigned long pullH;
  unsigned long pullL;
};

union UBXMessage {
  NAV_POSLLH pos;
  NAV_STATUS status;
  NAV_TIMEUTC time;
  MON_HW hwMonitor;
};

UBXMessage ubxMessage;

void calcChecksum(unsigned char* CK, int msgSize) {
  memset(CK, 0, 2);
  for (int i = 0; i < msgSize; i++) {
    CK[0] += ((unsigned char*)(&ubxMessage))[i];
    CK[1] += CK[0];
  }
}

boolean compareMsgHeader(const unsigned char* msgHeader) {
  unsigned char* ptr = (unsigned char*)(&ubxMessage);
  return ptr[0] == msgHeader[0] && ptr[1] == msgHeader[1];
}

int processGPS() {
  static int fpos = 0;
  static unsigned char checksum[2];

  static byte currentMsgType = MT_NONE;
  static int payloadSize = sizeof(UBXMessage);

  while (GPS_SERIAL.available()) {
    byte c = GPS_SERIAL.read();

    if (fpos < 2) {
      if (c == UBX_HEADER[fpos]) fpos++;
      else fpos = 0;
    } else {
      if ((fpos - 2) < payloadSize)
        ((unsigned char*)(&ubxMessage))[fpos - 2] = c;

      fpos++;

      if (fpos == 4) {
        if (compareMsgHeader(NAV_POSLLH_HEADER)) {
          currentMsgType = MT_NAV_POSLLH;
          payloadSize = sizeof(NAV_POSLLH);
        } else if (compareMsgHeader(NAV_STATUS_HEADER)) {
          currentMsgType = MT_NAV_STATUS;
          payloadSize = sizeof(NAV_STATUS);
        } else if (compareMsgHeader(NAV_TIMEUTC_HEADER)) {
          currentMsgType = MT_NAV_TIMEUTC;
          payloadSize = sizeof(NAV_TIMEUTC);
        } else if (compareMsgHeader(MON_HW_HEADER)) {
          currentMsgType = MT_MON_HW;
          payloadSize = sizeof(MON_HW);
        } else {
          fpos = 0;
          continue;
        }
      }

      if (fpos == (payloadSize + 2)) calcChecksum(checksum, payloadSize);
      else if (fpos == (payloadSize + 3)) {
        if (c != checksum[0]) fpos = 0;
      } else if (fpos == (payloadSize + 4)) {
        fpos = 0;
        if (c == checksum[1]) return currentMsgType;
      } else if (fpos > (payloadSize + 4)) fpos = 0;
    }
  }

  return MT_NONE;
}

// Temperature
#include <OneWire.h>
#include <DallasTemperature.h>

OneWire oneWire(WIRE_BUS);
DallasTemperature tempSensors(&oneWire);
DeviceAddress intTemp, ambTemp;

// SD
#include <SPI.h>
#include <SD.h>

// Main
byte lastLEDStatus = B000;

void reset(unsigned int delayTime) {
  setLEDs(B100);
  delay(delayTime);
  asm volatile ("  jmp 0");
}

void setLEDs(byte field) {
  lastLEDStatus = field;
  digitalWrite(LED_ERR, field & B100);
  digitalWrite(LED_WARN, field & B010);
  digitalWrite(LED_OK, field & B001);
}

void setup() {
  pinMode(LED_OK, OUTPUT);
  pinMode(LED_WARN, OUTPUT);
  pinMode(LED_ERR, OUTPUT);

  DEBUG_SERIAL.begin(9600);
  GPS_SERIAL.begin(9600);

  tempSensors.begin();

  setLEDs(B111);

  if (!tempSensors.getAddress(intTemp, 0)) {
    DEBUG_SERIAL.println("Could not find IntTemp");
    reset(5000);
  }

  if (!tempSensors.getAddress(ambTemp, 1)) {
    DEBUG_SERIAL.println("Could not find AmbTemp");
    reset(5000);
  }

  tempSensors.setResolution(intTemp, 8);
  tempSensors.setResolution(ambTemp, 8);

  if (!SD.begin(53)) {
    DEBUG_SERIAL.println("Could not initialize SD card");
    reset(5000);
  }

  GPS_SERIAL.write(portableCommand, sizeof(portableCommand));
}

void loop() {
  int msgType = processGPS();

  if (msgType == MT_NAV_POSLLH) {
    lat = ubxMessage.pos.lat;
    lon = ubxMessage.pos.lon;
    alt = ubxMessage.pos.altMSL;
  } else if (msgType == MT_NAV_STATUS) {
    fixStatus = ubxMessage.status.gpsFix | (ubxMessage.status.flags & B00000001) << 3;
  } else if (msgType == MT_MON_HW) {
    jammingIndicator = ubxMessage.hwMonitor.jamInd;
    noiseLevel = ubxMessage.hwMonitor.noisePerMS;
    antennaStatus = ubxMessage.hwMonitor.aStatus;
  } else if (msgType == MT_NAV_TIMEUTC) {
    sprintf(utcTime, "%02d%02d%04d%02d%02d%02d", ubxMessage.time.day, ubxMessage.time.month, ubxMessage.time.year, ubxMessage.time.hour, ubxMessage.time.minute, ubxMessage.time.second);
  }

  if (msgType != MT_NONE) lastGpsPacket = millis();

  if (millis() - lastUpdate >= 1000) {
    setLEDs(B000);

    if (millis() - lastGpsPacket > 10000) {
      fixStatus = 0;
      DEBUG_SERIAL.println("*** WARNING *** Last GPS packet received >10s ago");
      setLEDs(lastLEDStatus | B010);
    }

    tempSensors.requestTemperatures();
    float intTempC = tempSensors.getTempC(intTemp);
    float ambTempC = tempSensors.getTempC(ambTemp);

    DEBUG_SERIAL.print("$MOD");
    DEBUG_SERIAL.print(fixStatus, DEC);
    DEBUG_SERIAL.print(" LAT");
    DEBUG_SERIAL.print(lat / 10000000.0f, 8);
    DEBUG_SERIAL.print(" LON");
    DEBUG_SERIAL.print(lon / 10000000.0f, 8);
    DEBUG_SERIAL.print(" ALT");
    DEBUG_SERIAL.print(alt / 1000.0f, 8);
    DEBUG_SERIAL.print(" NOI");
    DEBUG_SERIAL.print(noiseLevel);
    DEBUG_SERIAL.print(" ANT");
    DEBUG_SERIAL.print(antennaStatus);
    DEBUG_SERIAL.print(" JAM");
    DEBUG_SERIAL.print(jammingIndicator / (255 / 100));
    DEBUG_SERIAL.print(" UTC");
    DEBUG_SERIAL.print(utcTime);
    DEBUG_SERIAL.print(" INT");
    DEBUG_SERIAL.print(intTempC);
    DEBUG_SERIAL.print(" AMB");
    DEBUG_SERIAL.print(ambTempC);
    DEBUG_SERIAL.println();

    File file = SD.open("log.txt", FILE_WRITE);
    if (file) {
      file.print("$MOD");
      file.print(fixStatus, DEC);
      file.print(" LAT");
      file.print(lat / 10000000.0f, 8);
      file.print(" LON");
      file.print(lon / 10000000.0f, 8);
      file.print(" ALT");
      file.print(alt / 1000.0f, 8);
      file.print(" NOI");
      file.print(noiseLevel);
      file.print(" ANT");
      file.print(antennaStatus);
      file.print(" JAM");
      file.print(jammingIndicator / (255 / 100));
      file.print(" UTC");
      file.print(utcTime);
      file.print(" INT");
      file.print(intTempC);
      file.print(" AMB");
      file.print(ambTempC);
      file.println();
      file.close();
    } else {
      DEBUG_SERIAL.println("*** ERROR *** Could not write to SD card");
      setLEDs(lastLEDStatus | B100);
    }

    if (fixStatus & B00001000) { // If location is valid
      setLEDs(lastLEDStatus | B001);

      if (alt > airborneAlt && !airborne) {
        DEBUG_SERIAL.println("*** INFO *** Switching to airborne mode");
        airborne = true;
        GPS_SERIAL.write(airborneCommand, sizeof(airborneCommand));
      } else if (alt < airborneAlt && airborne)  {
        DEBUG_SERIAL.println("*** INFO *** Switching to portable mode");
        airborne = false;
        GPS_SERIAL.write(portableCommand, sizeof(portableCommand));
      }
    }

    lastUpdate = millis();
  }
}

